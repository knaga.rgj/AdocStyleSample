
all: sample.pdfv

%.pdf:%.adoc
	asciidoctor-pdf  -r asciidoctor-diagram -r asciidoctor-mathematical $<
#	asciidoctor-pdf  -a pdf-stylesdir=adoc-style -a pdf-fontsdir=adoc-style -a pdf-theme=pdf-article-theme.yml -r asciidoctor-diagram -r asciidoctor-mathematical -a mathematical-format=svg $<


%-default.pdf:%.adoc
	asciidoctor-pdf -a scirpts=cjk -a pdf-theme=default-with-fallback-font -r asciidoctor-diagram -r asciidoctor-mathematical -a mathematical-format=svg -o $@ $<

%.pdfv:%.pdf
	qpdfview $<

%.xpdf:%.pdf
	qpdfview $<


jisx0213-2004-std.pdf: jisx0213-2004-std.adoc
jisx0213-2004-std.adoc: jisx0213-2004-std.txt jiscode.py
	python3 jiscode.py $<

jisx0213-2004-std.txt:
	wget http://x0213.org/codetable/jisx0213-2004-std.txt

clean:
	/bin/rm -f *~ stem-*svg adoc-style/*~
	/bin/rm -rf  .asciidoctor
	/bin/rm -f sample0.svg sin0.svg graph0.svg
