<!-- [GitLab版 GFM](https://docs.gitlab.com/ee/user/markdown.html)   -->
<!-- [GitHub版 GFM](https://github.github.com/gfm/) -->

# adoc-style sample

[adoc-style](https://gitlab.com/knaga.rgj/AdocStyle) を使用するサンプルです。

adoc-style は submodule 化してあり、adoc-style は GenX-NanSha を submodule 化しているので、
`git submodule update --init --recursive` してください。

Ubuntu/Debian で動作することを意図し、make で adoc -> pdf 変換してます。

Windows な人は WSL を活用してください :-)

例1: `make sample.pdf`  
例2: `make jisx0213-2004-std.pdf` (1〜2分くらいかかるかも)

