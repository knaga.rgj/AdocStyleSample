#!/usr/bin/python3

# http://x0213.org/codetable/
# http://x0213.org/codetable/iso-2022-jp-2004-std.txt
# http://x0213.org/codetable/jisx0213-2004-std.txt

import os
import re

pat1=re.compile(r'(\d-\S+)\s+(U\S+)\s(#.+)')


skips = ["3-2143"]
def dump(fname):
    ofname = os.path.splitext(fname)[0] + ".adoc"
    of = open(ofname, "w")
    of.write("\n= jis code\n K.Naga <knaga.rgj@gmail.com >\nv 0.5, 2021/02/22\n")
    of.write("include::adoc-style/pdf-article-attrs.adoc[]\n\n")
    of.write("from http://x0213.org/codetable/\n\nJIS X 0213 : http://x0213.org/codetable/jisx0213-2004-std.txt\n")
    of.write("[cols=\"2,1,1,1,1,8\"]\n|===\n")
    of.write("^|JIS ^|Regu ^|Bold ^|Ita ^|B&I ^|note\n\n")

    inf = open(fname, "r")
    for line in inf:
        if (len(line)==0) or (line[0]=='#'): continue
        res = pat1.match(line[:-1])
        if (res):
            if (res.group(1) in skips): continue
            of.write("|%s\n"%(res.group(1)))
            len2 = len(res.group(2))
            if (len2 == 6) or (len2 == 7):
                ch = chr(int(res.group(2)[2:], 16))
            elif (len2 == 11):
                ch = chr(int(res.group(2)[2:6], 16)) + chr(int(res.group(2)[7:], 16))
                #print("%s: u'%s'+u'%s'"
                #      %(res.group(1), res.group(2)[2:6], res.group(2)[7:]))
            else:
                print("%d: '%s'"%(len(res.group(2)), res.group(2)))
                ch = 'xxx'
                pass
            of.write("|%s\n"%(ch))
            of.write("|*%s*\n"%(ch))
            of.write("|_%s_\n"%(ch))
            of.write("|*_%s_*\n"%(ch))
            of.write("|%s\n"%(res.group(3)[2:]))
            of.write("\n")
                
        else:
            print('** '+line[:-1])
        pass
    
    of.write("|===\n")

    inf.close()
    of.close()
    return



if __name__ == '__main__':
    import sys
    dump(sys.argv[1])
    pass

